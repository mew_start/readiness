# gitlab-shell on Kubernetes

## Summary

Currently git-ssh and
[alt-ssh](https://about.gitlab.com/blog/2016/02/18/gitlab-dot-com-now-supports-an-alternate-git-plus-ssh-port/)
traffic is hosted via a fleet of git servers.  This document will cover the
aspects of shifting this workload from VM's onto Kubernetes. This work is being
done as part of the Delivery Teams' OKR which is tracked in
https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112

The result of this move will include quicker deployment times through use of
Kubernetes as the deployment mechanism for the related objects, `gitlab-shell`
and `webservice`.  All metrics related to the `git` service must not change.
This work may include a reduction in cost as an entire fleet of VM's will be
removed in favor of placing the workload in a more densely packed set of servers
operating inside of the Kubernetes cluster.

## Architecture

### Current

```mermaid
sequenceDiagram
  participant C as Client
  participant H as HAProxy
  participant gs as GitLab_Shell
  participant w as Workhorse
  participant r as Rails
  participant gi as Gitaly

  C->>H: git-clone
    Note over C,H: git clone git@gitlab.com:project/repo.git
  H->>gs: git-clone
  gs->>w: [authenticate]
    Note over gs,w: communication via unix socket
  w->>r: GET /api/v4/internal/authorized_keys
    Note over w,r: communication via API using localhost puma
  r->>w: 
  w->>gs: [authenticate success]
  gs->>gi: command: git-upload-pack
  gi->>gs: SSHUploadPack
  gs->>H: git data
  H->>C: git data
```

### Post Migration

```mermaid
sequenceDiagram
  participant C as Client
  participant H as HAProxy
  participant gs as GitLab_Shell
  participant w as Workhorse
  participant r as Rails
  participant gi as Gitaly

  C->>H: git-clone
    Note over C,H: git clone git@gitlab.com:project/repo.git
  H->>gs: git-clone
    Note over H,gs: communication through GLB/nginx Ingress
  gs->>w: [authenticate]
    Note over gs,w: communication through Kubernetes Service to webservice endpoint
  w->>r: GET /api/v4/internal/authorized_keys
    Note over w,r: communication via API http://gitlab-webservice:8181
  r->>w: 
  w->>gs: [authenticate success]
  gs->>gi: command: git-upload-pack
  gi->>gs: SSHUploadPack
  gs->>H: git data
  H->>C: git data
```

---

The movement into Kubernetes introduces a few new network hops that are not seen
in the VM infrastructure.  These consist of the following:

* Between HAProxy and `gitlab-shell` is a hop through a Google Load Balancer (GLB) to send the traffic
  to the Kubernetes cluster.  In our VM infrastructure, haproxy will send
  traffic directly to a chosen `git` server.
* The `gitlab-shell` does not run workhorse locally, therefore requests are sent
  to another endpoint for authentication attempts.  This endpoint is another
  service running inside of GKE, in this case the `gitlab-webservice`.  In our
  VM Infrastructure, the same server that sees that SSH request will speak via
  `localhost` to Workhorse.

In Kubernetes, the `gitlab-shell` Deployment only runs the `ssh` daemon while the
`webservice` Deployment runs 2 containers, one for Puma, and one for
Workhorse.

For authentication attempts, `gitlab-shell` will reach out to the `webservice`
Deployment, specifically Workhorse.  After authentication is successful,
`gitlab-shell` will forward requests to our Gitaly fleet to retrieve the desired
data.

### Network Firewall

All of the following rules are configured via Kubernetes Network Policy Objects
built into our helm chart.

#### Ingress

* Port 2222 is the inbound port which `sshd` is listening on

#### Egress

* Port 8181 to speak to `webservice` for Workhorse
* Port 9999 to speak to Gitaly
* Port 53 for DNS

## Operational Risk Assessment

### Operational Scale

This service will operate on it's own node pool on staging and production for
the main stages.  For preprod and the canary stage of production, they'll run on
our default node pool.  Due to the performance characteristics observed by this
service (discussed later in this document), we are using `n1-highcpu` instance
types.

This service will use an Horizontal Pod Autoscaler (HPA) to control the amount
of Pods running at any given time.  The rollout plan will over-provision this
service, and later revisit the performance of this service to determine how to
adjust the amount of Pods running that can handle traffic.

SSH contains a configuration option called `MaxStartups`:

> Specifies the maximum number of concurrent unauthenticated connections to the
> SSH daemon. Additional connections will be dropped until authentication
> succeeds or the LoginGraceTime expires for a connection. The default is 10.
> Alternatively, random early drop can be enabled by specifying the three colon
> separated values start:rate:full (e.g. "10:30:60").  sshd(8) will refuse
> connection attempts with a probability of rate/100 (30%) if there are
> currently start (10) unauthenticated connections.  The probability increases
> linearly and all connection attempts are refused if the number of
> unauthenticated connections reaches full (60).

Source: `man sshd_config`

This is currently customized on our VM infrastructure with a configuration of
`200:60:400`, we will carry this same configuration option going into our
Kubernetes Infrastructure.  We can revisit this configuration at a later point
in time if necessary.

### Maintenance Tasks

During some maintenance tasks such as cluster and node upgrades, we'll rely on
the ability for Kubernetes to read the Pod Disruption Budget (PDB) in order to
prevent too many Pods from going down at any given point in time.  We default
our PDB to 1, meaning only 1 Pod is allowed to go down during a maintenance
event.

During deployments, we utilize our default Rolling Strategy with a `maxSurge` of
25% and `maxUnavailable` of 25%.

### Internal Dependencies

* Workhorse - provided by the `gitlab-webservice` Kubernetes Deployment
  * Should this service experience a failure, customers will be unable to
    authenicate leading to failures of interacting with this service.
* Gitaly - provided by VM's using the `gprd-base-stor` chef role
  * Should this service experience a failure, customers will not be able to
    interact with thier repositories.
* HAProxy - sends traffic to this service
  * Should this service experience a failure we will not be able to receive
    traffic from our customers.
  * We receive traffic from one set of servers `fe-*`.  The
    failure scenario for each would be the same.

This service does not depend on anything else.  Being that it's only a proxy to
workhorse and gitaly, there's no reliance on Redis nor Postgresql.  Workhorse,
however, does rely these stateful services.  A failure of these systems would
negatively impact Workhorse, in which that failure scenario is described above.

### External Dependencies

* dev.gitlab.org - provides the Container Images utilized for deployments
  * Should we experience a failure here, we'll only experience an interruption
    during deploys.  Kubernetes will protect us as new Pods will fail to start
    up and the Deployment will be noted as a failure and be automatically rolled
    back.
* Google Kubernetes Engine - provides the infrastructure this service runs
  inside
  * If GKE goes down, the failures will vary.  Should we lose the ability to
    talk to our cluster, but the cluster remains functional, we should not
    experience any service disruption.  If a cluster itself goes down, it would
    be a severe disruption and should be treated as such.
  * Note we are working towards cluster redundancy per zone:
    https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1175

### Network Topology

The network configuration and communication of all components will look very
similar to that of the https-git migration.  [Review our readiness guide for
http-git](https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/git-https-websockets#gke-network-topology-for-webservice)
for additional details.

### Operational Rollout

This service will be tested prior to implementing it to production.  We can also
slowly roll traffic onto this service by leveraging weights with HAProxy.  This
will help us safely monitor the performance of this service to ensure we are not
negatively impacting customer interactions.

#### Migration Plan

The above plan will be executed in similar fashion to how our http-git traffic
has been migrated.  [Review our readiness guide for
http-git](https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/git-https-websockets#migration-plan)
for additional details.

### Failure Scenario

A catastrophic failure of this service would prevent any operations using ssh
from working properly.  A work around would be to switch to https git
operations. This may not be feasible for all use-cases.  An outage of this
service is a severe degradation to our platform and should be treated
accordingly.

### Pod Failure Scenarios

The Pod runs three processes:

* PID 1: /bin/sh -c "/scripts/process-wrapper"
* tail -f /var/log/gitlab-shell/gitlab-shell.log
* /usr/sbin/sshd -E /var/log/gitlab-shell/ssh

When Kubernetes needs to terminate a Pod for the purposes of a Deploy, the Pod
fails to accept a `SIGTERM`, leaving the Pod in an state of `Terminating` until
Kubernetes hits the `terminationGracePeriod`, which defaults to 30 seconds.
This is not great, but prevents us from immediately terminating any active
sessions to the Pod, and should allow a cleaner deployment for user facing
connectivity.  Currently the `terminationGracePeriod` is not a configurable
option: https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2356

The healthcheck for the Pod is a simple `pgrep sshd`.  This is problematic as
clients that connect to the Pod will match this healthcheck.  Should the sshd
service fail for X reason, clients that are connected will allow the healthcheck
to pass, despite the fact that ssh is no longer listening.  Because the
healthcheck passes, the Pod is not removed from rotation, and will continue to
accept incoming connections, which will immediately fail.  Example failure
message:

```
% git fetch minikube
ssh: connect to host gitlab.172.17.0.3.nip.io port 32022: Connection refused
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

Issue to improve this is here: https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2357

The ssh service is relatively stable, the likely hood of this happening should
be rare.  However, we lack metrics for how long connections last to sshd, so we
do not technically have visibility into how long a failed Pod might potentially
run before some intervention is required.

## Database

No changes to the operational nature of our database systems with this
migration.

## Security and Compliance

The security stature of this service does not change.  The scope of managing
security, however, will change as this is a shift in where the service is
running (Virtual Machines vs Kubernetes).  This service will receive security
updates in accordance to the [GitLab Release and Maintenance
Policy](https://docs.gitlab.com/ee/policy/maintenance.html)

### Application Upgrade and Rollback

The deployment mechanism for the GitLab-Shell will mirror that of all other
services which utilize our own helm chart.  Please see our [existing Kubernetes
deployment
documentation.](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/gitlab-com-deployer.md#kubernetes-upgrade)

Further research is required to determine what happens during a deploy (as well
as failure scenarios) as a Pod is replaced.  See [issue
1285](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1285)

### Cluster Upgrades

We moving towards a more automated approach towards cluster upgrades to ensure
we stay on top of security improvements to the clusters that this services will
operate on.  More details of this can be found in
[delivery#1137](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1137)

## Performance

### Current VM Setup

Using our [haproxy
dashboard](https://dashboards.gitlab.net/d/ZOOh_aNik/haproxy?orgId=1&refresh=5m&var-env=gprd&var-host=All&var-port=9101&var-backend=ssh&var-frontend=All&var-server=All&var-code=All&from=now-24h&to=now)
we can derive a few points that should be maintained after the service is rolled
into production and prior to accepting traffic.

* The average healthcheck takes only 12ms
  * The caveat here is that the healthcheck will need to change when migrating
    over into Kubernetes as we check the puma worker on our git fleet, not the
    fact that ssh is up and running
  * Also keep in mind, in the eyes of HAProxy, it's going to send one
    healthcheck that reaches one random pod instead of checking each Pod
      * Though this should not be an issue as Kubernetes will only place Pods
        that are in Ready state into the Service utilized by HAproxy
* The VM's experience between 400-1000 sessions per second, this is throughout
  the day across the entire fleet.
* The rate at which haproxy ever retries requests to any backend is low,
  hovering below 0.05 requests per second.
* We _never_ build a queue as items are processed quickly enough, or minimally
  sent directly to a backend immediately.

Using our [ssh performance
dashboard](https://dashboards.gitlab.net/d/-UvftW1iz/ssh-performance)
we can derive a few additional points along with the above:

* The vast majority of request durations fall below 50 seconds of time
  * We have a timeout of 10 minutes
  * This view should not change


### Kubernetes Configuration

Using our [ssh process
dashboard](https://dashboards.gitlab.net/d/git-main/git-overview?orgId=1) we can
derive a prediction for how our Pods should be configured:

* Across 24 VM's, we appear to hover around the 500MB mark with high load
  reaching 1GB of memory usage.  With this assuming at least 24 Pods, we should
  request 500M of memory and limit ourselves to 1.5GB of memory.

* Our CPU usage appears to average at about 150 millicores, but our max usage is
  high, usually around 1 core with some spikes at 2 cores.  With at least 24
  Pods, we should request at least 150m, and for each Pod, we'll set a max of 3
  cores to avoid heavy throttling

These are only starting values.  As we migrate traffic over, we'll monitor the
usage and performance characteristics of this deployment and tweak as necessary.

### Throttling

Throttling of this service is handled in two formats.  One is excessive
connection attempts per Pod.  This is done via the `MaxStartups` option as noted
earlier.  If one Pod is overwhelmed with connections, they'll be dropped.  The
customer will need to retry and hope to land on a Pod that is not overly
saturated with connections.  As noted later, we do not have visibility into this
detail.  We currently do not have a way to mitigate these drops as HAProxy will
send it to the only backend available, which Kubernetes will route the traffic
to any Running and Ready Pod.  This is due to a lack of visibility provided by
the ssh daemon.

Additional throttling is at the HAProxy TCP Layer 4 stack.  We limit sessions to
60 using `rate-limit sessions <rate>`:

> The <rate> parameter is an integer designating the maximum number of new
> sessions per second to accept on the frontend.

This will queue incoming connections per front-end node when we exceed a rate of
60 new connections per second per front end node.  Hitting this limit will
result in a slight delay of that request before haproxy sends it to the backend.

## Backup and Restore

This is a stateless component of GitLab.  No work to be done regarding backups
and restoration of customer data with this migration.

## Monitoring and Alerts

### Logging

`gitlab-shell` has two log files, one from the `gitlab-shell` and another from
the `ssh` daemon.  Both files are output into stdout.  Only the logs from
`gitlab-shell` are formatted as JSON.  All data is sent to Elastic Search under
our existing indices `pubsub-shell-inf-<ENV>`.  Due to this additional
unstructured logging polluting our Elasticsearch cluster.

Because we are using `tail` to monitor our log files for `fluentd` to consume,
we'll have a lot of garbage data such as new lines, and indicators from `tail`
as it switches which log file is currently being displayed.  Some very rough
math indicates that this will increase the log data held by Elastic Search by
70GB.  Details about this investigation can be found here: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1275#note_430335199

The current trick for quickly noting which logs you may want to observe, is to
filter based on whether we are popluating the `msg` or `message` fields.

* `gitlab-shell` will populate `json.msg`
* `sshd` outputs to unstructured stdout where fluentd will take an event and
  populate `json.message`.

We can then adjust filters looking for whether either field exists to determine
which logs we choose to observe.  In our non-prod events:

* GitLab-Shell Logs: https://nonprod-log.gitlab.net/goto/76570bf241a5f362f107c94d13b64c21
* sshd Logs: https://nonprod-log.gitlab.net/goto/ad6039dbb81dd271204e1cd66c981031

### Monitoring

On our VM's we are running `mtail` which reads our log data to provide metrics
available for Prometheus.  This is not something that is currently available to
our Containers, nor our Omnibus installations.

https://gitlab.com/gitlab-org/gitlab-shell/-/issues/121

It's been determined that we do not have any alerts or monitoring currently
reliant on the mtail metrics we gather.  Therefore, this has been decided to NOT
be a blocker for moving the service forward.  Issue
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1274 has been created
to address this until a solution is provided via the `gitlab-shell` team as
noted above.

Our haproxy loadbalancers currently are the next best place to watch for metrics
related to this service.  We'll mostly see traffic patterns vs service health
related metrics, however.

https://dashboards.gitlab.net/d/ZOOh_aNik/haproxy?orgId=1&refresh=5m&var-env=gprd&var-host=All&var-port=9101&var-backend=ssh&var-frontend=All&var-server=All&var-code=All


Currently we lack a dedicated dashboard for the `gitlab-shell` service.  This is
to be added via: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1273

- [ ] **Do we have a target SLA in place for this service?**
- [ ] **Do we know what the indicators (SLI) are that map to the target SLA?**
- [ ] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**
- [ ] **Do we have troubleshooting runbooks linked to these alerts?**
- [ ] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

### Alerting

We currently have a wide scope of monitoring that comes from the use of
`kube-state-metrics`.  We'll continue to leverage our exiting alerting rules to
warn us of problems related to the operational state of these Pods from the
scope of running inside of Kubernetes.

We also have a suite of metrics specific monitor the ssh backend in haproxy
which will alert us if we see excessive failed connection failures.

## Responsibility

SME's:

* Infrastructure:
  * Delivery
  * Scalability
  * Reliability
* Deployment: ~team::Delivery
* Helm Chart: ~team::Distribution
* GitLab Shell:
  * ~group::source code
  * https://gitlab.com/gitlab-org/gitlab-shell

## Testing

No testing was performed as this is a service that already exists in our
environment.  All testing has been circled around the migration of this service
into Kubernetes.

## Readiness review participants

* [@ahanselka](https://gitlab.com/ahanselka)
* [@ggillies](https://gitlab.com/ggillies)
* [@andrewn](https://gitlab.com/andrewn)
